module.exports = (api) => {
	return {
		presets: [
			[
				"@babel/preset-env",
				{
					corejs: { version: "3.19" },
					useBuiltIns: "usage",
				},
			],
			["@babel/preset-typescript", { jsxPragma: "h" }],
		],
		plugins: [
			[
				"@babel/plugin-transform-react-jsx",
				{
					runtime: "automatic",
					importSource: "preact",
				},
			],
			api.env("development") ? "@prefresh/babel-plugin" : null,
		].filter(Boolean),
	};
};
