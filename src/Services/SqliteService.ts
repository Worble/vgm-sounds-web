import { v4 } from "uuid";
import { QueryExecResult, SqlValue } from "sql.js";

export default class SqliteService {
	private worker: Worker;
	constructor(worker: Worker) {
		this.worker = worker;
	}

	private queryDb = <T>(
		exec: { sql: string; params?: object },
		map: (vals: SqlValue[]) => T
	): Promise<T[]> => {
		return new Promise((resolve, reject) => {
			const requestId = v4();

			this.worker.onmessage = (e) => {
				if (e.data.id == requestId) {
					if (e.data.error) {
						reject(e.data.error);
						return;
					}
					if (!e.data.results || e.data.results.length < 1) {
						resolve([]);
						return;
					}
					const results = e.data.results[0] as QueryExecResult;
					resolve(results.values.map(map));
				}
			};

			this.worker.postMessage({
				id: requestId,
				action: "exec",
				sql: exec.sql,
				params: exec.params,
			});
		});
	};

	getGames = (): Promise<IGame[]> => {
		return this.queryDb({ sql: "SELECT g.Id, g.Name FROM Games g" }, (vals) => {
			return { Id: vals[0] as number, Name: vals[1] as string };
		});
	};

	getGame = async (gameId: number): Promise<IGame> => {
		const query = await this.queryDb(
			{
				sql: `SELECT g.Id, g.Name 
FROM Games g
WHERE g.Id = $gameId`,
				params: { $gameId: gameId },
			},
			(vals) => {
				return { Id: vals[0] as number, Name: vals[1] as string };
			}
		);

		if (query[0]) {
			return query[0];
		} else {
			throw new Error("Query returned no results");
		}
	};

	getProducts = (): Promise<IProduct[]> => {
		return this.queryDb({ sql: "SELECT p.Id, p.Name FROM Products p" }, (vals) => {
			return { Id: vals[0] as number, Name: vals[1] as string };
		});
	};

	getProduct = async (productId: number): Promise<IProduct> => {
		const query = await this.queryDb(
			{
				sql: `SELECT p.Id, p.Name
FROM Products p
WHERE p.Id = $productId`,
				params: { $productId: productId },
			},
			(vals) => {
				return { Id: vals[0] as number, Name: vals[1] as string };
			}
		);

		if (query[0]) {
			return query[0];
		} else {
			throw new Error("Query returned no results");
		}
	};

	getSamplesForGame = (gameId: number): Promise<IGameSample[]> => {
		return this.queryDb(
			{
				sql: `
SELECT s.Id, s.Notes, s.Examples, s.ProductId, p.Name, pr.Name, bp.Name 
FROM Samples s
JOIN Products p on s.ProductId = p.Id
JOIN Programs pr on s.ProgramId = pr.Id
JOIN PathBanks bp on s.PathBankId = bp.Id
where s.GameId = $gameId`,
				params: { $gameId: gameId },
			},
			(vals) => {
				return {
					Id: vals[0] as number,
					Notes: vals[1] as string,
					Examples: vals[2] as string,
					ProductId: vals[3] as number,
					ProductName: vals[4] as string,
					Program: vals[5] as string,
					PathBank: vals[6] as string,
				};
			}
		);
	};

	getSamplesForProduct = (productId: number): Promise<IProductSample[]> => {
		return this.queryDb(
			{
				sql: `
SELECT s.Id, s.Notes, s.Examples, s.GameId, g.Name, p.Name, bp.Name 
FROM Samples s
JOIN Games g on s.GameId = g.Id
JOIN Programs p on s.ProgramId = p.Id
JOIN PathBanks bp on s.PathBankId = bp.Id
where s.ProductId = $productId`,
				params: { $productId: productId },
			},
			(vals) => {
				return {
					Id: vals[0] as number,
					Notes: vals[1] as string,
					Examples: vals[2] as string,
					GameId: vals[3] as number,
					GameName: vals[4] as string,
					Program: vals[5] as string,
					PathBank: vals[6] as string,
				};
			}
		);
	};
}

export interface IGame {
	Id: number;
	Name: string;
}

export interface IProduct {
	Id: number;
	Name: string;
}

export interface ISample {
	Id: number;
	Notes: string;
	Examples: string;
	Program?: string;
	PathBank?: string;
}

export interface IGameSample extends ISample {
	ProductId: number;
	ProductName: string;
}

export interface IProductSample extends ISample {
	GameId: number;
	GameName: string;
}
