import { FunctionComponent, useContext, useEffect, useState } from "react";
import { Alert, Container, Spinner } from "react-bootstrap";
import { IGame, IProduct } from "../Services/SqliteService";
import { SqliteServiceContext } from "../Contexts/SqliteServiceContext";

type HomeState =
	| { type: "Loading" }
	| { type: "Loaded"; games: IGame[]; products: IProduct[] }
	| { type: "Error"; error: string };

const Home: FunctionComponent = () => {
	const [state, setState] = useState<HomeState>({ type: "Loading" });
	const sqliteService = useContext(SqliteServiceContext)!;

	useEffect(() => {
		if (sqliteService.type === "Loaded") {
			(async () => {
				try {
					setState({
						type: "Loaded",
						games: await sqliteService.service.getGames(),
						products: await sqliteService.service.getProducts(),
					});
				} catch (e) {
					setState({ type: "Error", error: (e as Error).toString() });
				}
			})();
		}
	}, [sqliteService]);

	switch (state.type) {
		case "Loading":
			return (
				<Container>
					<Spinner animation="border" />
				</Container>
			);
		case "Error":
			return (
				<Container>
					<Alert variant="danger">{state.error}</Alert>
				</Container>
			);
		case "Loaded":
			return (
				<Container>
					<div>
						<h2>Games</h2>
						{state.games.map((game) => {
							return (
								<ul key={game.Id}>
									<a href={`/games/${game.Id}`}>
										<li>{game.Name}</li>
									</a>
								</ul>
							);
						})}
					</div>
					<br />
					<div>
						<h2>Products</h2>
						{state.products.map((product) => {
							return (
								<ul key={product.Id}>
									<a href={`/products/${product.Id}`}>
										<li>{product.Name}</li>
									</a>
								</ul>
							);
						})}
					</div>
				</Container>
			);
	}
};

export default Home;
