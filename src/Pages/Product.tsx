import { FunctionComponent, useContext, useEffect, useState } from "react";
import { useRouter } from "preact-router";
import { SqliteServiceContext } from "../Contexts/SqliteServiceContext";
import { IProduct, IProductSample } from "../Services/SqliteService";
import Spinner from "react-bootstrap/Spinner";
import { Alert, Breadcrumb, BreadcrumbItem, Card, Container, Stack, Table } from "react-bootstrap";
import React from "preact/compat";
import { groupBy } from "lodash-es";

const Display: FunctionComponent<{ samples: IProductSample[] }> = (props) => {
	const groups = groupBy(props.samples, (e) => e.GameId);
	const cards = Object.entries(groups).map(([gameId, group]) => {
		const gameName = group.find((e) => e.GameId === Number.parseInt(gameId))!.GameName;
		return (
			<Card>
				<Card.Header>
					<a href={`/games/${gameId}`}>
						<h3>{gameName}</h3>
					</a>
				</Card.Header>
				<Card.Body>
					<Table>
						<thead>
							<tr>
								<td>Path/Bank</td>
								<td>Program</td>
								<td>Examples</td>
								<td>Notes</td>
							</tr>
						</thead>
						<tbody>
							{group.map((e) => {
								return (
									<tr>
										<td>{e.PathBank}</td>
										<td>{e.Program}</td>
										<td>{e.Examples}</td>
										<td>{e.Notes}</td>
									</tr>
								);
							})}
						</tbody>
					</Table>
				</Card.Body>
			</Card>
		);
	});
	return <Stack gap={4}>{cards}</Stack>;
};

type ProductState =
	| { type: "Loading" }
	| { type: "Loaded"; product: IProduct; samples: IProductSample[] }
	| { type: "Error"; error: string };

const Product: FunctionComponent<{ id?: string }> = (props) => {
	const [, route] = useRouter();
	if (!props.id || Number.isNaN(Number.parseInt(props.id))) {
		route("/");
		return null;
	}
	const productId = Number.parseInt(props.id);

	const [state, setState] = useState<ProductState>({ type: "Loading" });
	const sqliteService = useContext(SqliteServiceContext)!;

	useEffect(() => {
		if (sqliteService.type === "Loaded") {
			(async () => {
				try {
					setState({
						type: "Loaded",
						product: await sqliteService.service.getProduct(productId),
						samples: await sqliteService.service.getSamplesForProduct(productId),
					});
				} catch (e) {
					setState({ type: "Error", error: (e as Error).toString() });
				}
			})();
		}
	}, [sqliteService]);

	let el = null;

	switch (state.type) {
		case "Loading":
			el = <Spinner animation="border" />;
			break;

		case "Error":
			el = <Alert variant="danger">{state.error}</Alert>;
			break;
		case "Loaded":
			el = <Display samples={state.samples} />;
			break;
	}

	return (
		<Container>
			<Breadcrumb>
				<BreadcrumbItem href="/">Home</BreadcrumbItem>
				<BreadcrumbItem active={true}>
					{state.type === "Loaded" ? state.product.Name : "Product"}
				</BreadcrumbItem>
			</Breadcrumb>
			<h1>{state.type === "Loaded" ? state.product.Name : "Product"}</h1>
			{el}
		</Container>
	);
};

export default Product;
