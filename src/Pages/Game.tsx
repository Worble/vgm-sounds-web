import { FunctionComponent, useContext, useEffect, useState } from "react";
import { Alert, Breadcrumb, BreadcrumbItem, Card, Container, Stack, Table } from "react-bootstrap";
import { IGame, IGameSample } from "../Services/SqliteService";
import { useRouter } from "preact-router";
import Spinner from "react-bootstrap/Spinner";
import React from "preact/compat";
import { SqliteServiceContext } from "../Contexts/SqliteServiceContext";
import { groupBy } from "lodash-es";

const Display: FunctionComponent<{ samples: IGameSample[] }> = (props) => {
	const groups = groupBy(props.samples, (e) => e.ProductId);
	const cards = Object.entries(groups).map(([productId, group]) => {
		const productName = group.find(
			(e) => e.ProductId === Number.parseInt(productId)
		)!.ProductName;
		return (
			<Card>
				<Card.Header>
					<a href={`/products/${productId}`}>
						<h3>{productName}</h3>
					</a>
				</Card.Header>
				<Card.Body>
					<Table>
						<thead>
							<tr>
								<td>Path/Bank</td>
								<td>Program</td>
								<td>Examples</td>
								<td>Notes</td>
							</tr>
						</thead>
						<tbody>
							{group.map((e) => {
								return (
									<tr>
										<td>{e.PathBank}</td>
										<td>{e.Program}</td>
										<td>{e.Examples}</td>
										<td>{e.Notes}</td>
									</tr>
								);
							})}
						</tbody>
					</Table>
				</Card.Body>
			</Card>
		);
	});
	return <Stack gap={4}>{cards}</Stack>;
};

type GameState =
	| { type: "Loading" }
	| { type: "Loaded"; game: IGame; samples: IGameSample[] }
	| { type: "Error"; error: string };

const Game: FunctionComponent<{ id?: string }> = (props) => {
	const [, route] = useRouter();
	if (!props.id || Number.isNaN(Number.parseInt(props.id))) {
		route("/");
		return null;
	}
	const gameId = Number.parseInt(props.id);

	const [state, setState] = useState<GameState>({ type: "Loading" });
	const sqliteService = useContext(SqliteServiceContext)!;

	useEffect(() => {
		if (sqliteService.type === "Loaded") {
			(async () => {
				try {
					setState({
						type: "Loaded",
						game: await sqliteService.service.getGame(gameId),
						samples: await sqliteService.service.getSamplesForGame(gameId),
					});
				} catch (e) {
					setState({ type: "Error", error: (e as Error).toString() });
				}
			})();
		}
	}, [sqliteService]);

	let el = null;

	switch (state.type) {
		case "Loading":
			el = <Spinner animation="border" />;
			break;

		case "Error":
			el = <Alert variant="danger">{state.error}</Alert>;
			break;
		case "Loaded":
			el = <Display samples={state.samples} />;
			break;
	}

	return (
		<Container>
			<Breadcrumb>
				<BreadcrumbItem href="/">Home</BreadcrumbItem>
				<BreadcrumbItem active={true}>
					{state.type === "Loaded" ? state.game.Name : "Game"}
				</BreadcrumbItem>
			</Breadcrumb>
			<h1>{state.type === "Loaded" ? state.game.Name : "Game"}</h1>
			{el}
		</Container>
	);
};

export default Game;
