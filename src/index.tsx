import { render } from "preact";
import App from "./App";
import "styles/index.scss";

const app = document.createElement("div");
render(<App />, app);
document.body.appendChild(app);
