import { FunctionComponent } from "preact";
import { SqliteServiceProvider } from "./Contexts/SqliteServiceContext";
import React, { Suspense } from "preact/compat";
import Router, { Route } from "preact-router";
import { lazy } from "react";
import Spinner from "react-bootstrap/Spinner";
import { Container } from "react-bootstrap";
import Product from "./Pages/Product";

const Home = lazy(() => import("Pages/Home"));
const Game = lazy(() => import("Pages/Game"));

const App: FunctionComponent = () => {
	return (
		<React.StrictMode>
			<SqliteServiceProvider>
				<Router>
					<Route
						path="/"
						component={() => (
							<Suspense
								fallback={
									<Container>
										<Spinner animation="border" />
									</Container>
								}
							>
								<Home />
							</Suspense>
						)}
					/>
					<Route
						path="/games/:id"
						component={(props) => (
							<Suspense
								fallback={
									<Container>
										<Spinner animation="border" />
									</Container>
								}
							>
								<Game {...props} />
							</Suspense>
						)}
					/>
					<Route
						path="/products/:id"
						component={(props) => (
							<Suspense
								fallback={
									<Container>
										<Spinner animation="border" />
									</Container>
								}
							>
								<Product {...props} />
							</Suspense>
						)}
					/>
				</Router>
			</SqliteServiceProvider>
		</React.StrictMode>
	);
};

export default App;
