import { createContext, FunctionComponent, useEffect, useState } from "react";
import SqliteService from "../Services/SqliteService";
import { Alert, Container } from "react-bootstrap";

const worker = new Worker(new URL("sql.js/dist/worker.sql-wasm.js", import.meta.url));

type SqliteServiceContextState =
	| { type: "Loading" }
	| { type: "Loaded"; service: SqliteService }
	| { type: "Error"; error: string };
const SqliteServiceContext = createContext<SqliteServiceContextState>({ type: "Loading" });

const SqliteServiceProvider: FunctionComponent = (props) => {
	const [service, setService] = useState<SqliteServiceContextState>({ type: "Loading" });

	worker.onmessage = (e) => {
		if (e.data.id == "open_db") {
			if (e.data.error) {
				setService({ type: "Error", error: e.data.error });
				return;
			}

			setService({ type: "Loaded", service: new SqliteService(worker) });
		}
	};

	useEffect(() => {
		(async () => {
			const buf = await (
				await fetch(new URL(process.env["DB_PATH"]!, window.location.href).toString())
			).arrayBuffer();
			worker.postMessage({
				id: "open_db",
				action: "open",
				buffer: buf,
			});
		})();
	}, []);

	return (
		<SqliteServiceContext.Provider value={service}>
			{service.type === "Error" && (
				<Container>
					<Alert variant="danger">{service.error}</Alert>
				</Container>
			)}
			{props.children}
		</SqliteServiceContext.Provider>
	);
};

export { SqliteServiceContext, SqliteServiceProvider };
