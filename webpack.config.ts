import HtmlWebpackPlugin from "html-webpack-plugin";
import MiniCssExtractPlugin from "mini-css-extract-plugin";
import * as path from "path";
import * as webpack from "webpack";
import "webpack-dev-server";
import CssMinimizerPlugin from "css-minimizer-webpack-plugin";
//@ts-ignore
import PreactRefreshPlugin from "@prefresh/webpack";
import ForkTsCheckerWebpackPlugin from "fork-ts-checker-webpack-plugin";

const Dotenv = require("dotenv-webpack");

const CopyPlugin = require("copy-webpack-plugin");

const config: ({}, argv: webpack.WebpackOptionsNormalized) => webpack.Configuration = (
	{},
	argv
) => {
	const isDev = argv.mode === "development";

	return {
		target: isDev ? "web" : "browserslist",
		entry: path.resolve(__dirname, "src", "index.tsx"),
		resolve: {
			extensions: [".tsx", ".ts", ".js"],
			alias: {
				react: "preact/compat",
				"react-dom/test-utils": "preact/test-utils",
				"react-dom": "preact/compat",
				"react/jsx-runtime": "preact/jsx-runtime",
			},
			modules: [path.resolve(__dirname, "src"), "node_modules"],
			fallback: {
				crypto: require.resolve("crypto-browserify"),
				path: require.resolve("path-browserify"),
				buffer: require.resolve("buffer/"),
				stream: require.resolve("stream-browserify"),
				fs: require.resolve("browserify-fs"),
				util: require.resolve("util/"),
			},
		},
		module: {
			rules: [
				{
					test: /\.[tj]sx?$/,
					use: {
						loader: "babel-loader",
					},
					exclude: /node_modules/,
				},
				{
					test: /\.(c|sa|sc)ss$/i,
					use: [
						isDev ? "style-loader" : MiniCssExtractPlugin.loader,
						"css-loader",
						{
							loader: "sass-loader",
							options: {
								sassOptions: {
									includePaths: ["node_modules/bootstrap/scss"],
								},
							},
						},
					],
				},
			],
		},
		devtool: isDev ? "inline-source-map" : "source-map",
		plugins: [
			new MiniCssExtractPlugin({
				filename: "[name].[contenthash].css",
				chunkFilename: "[id].[contenthash].css",
			}),
			new HtmlWebpackPlugin({
				hash: true,
				cache: true,
				minify: "auto",
				showErrors: true,
				scriptLoading: "module",
				title: "VGM Sounds",
			}),
			new ForkTsCheckerWebpackPlugin({
				typescript: {
					diagnosticOptions: {
						semantic: true,
						syntactic: true,
					},
				},
			}),
			isDev ? new PreactRefreshPlugin() : null,
			new CopyPlugin({
				patterns: ["node_modules/sql.js/dist/sql-wasm.wasm", "static"],
			}),
			new Dotenv(),
		].filter(Boolean),
		optimization: {
			minimizer: [`...`, new CssMinimizerPlugin()],
		},
		output: {
			path: path.resolve(__dirname, "dist"),
			filename: "[name].[contenthash].js",
			clean: true,
			publicPath: "/",
		},
		experiments: {
			asyncWebAssembly: true,
			syncWebAssembly: true,
		},
	};
};

export default config;
